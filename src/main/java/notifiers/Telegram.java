package notifiers;

import discovery.Notifier;

public class Telegram implements Notifier {
	
	private String notifierName = "Telegram";

    @Override
    public String notify(String message) {
        String msj = "Notificación enviada por Telegram: " + message;
        return msj;
    }

	@Override
	public String getName() {
		return notifierName;
	}
}
